const http = require('http');
const querystring = require('querystring');

calculatePrices({
    selectedCart: getSelectedCart(),
    currencies: ['EUR', 'JPY', 'RUB', 'GBP'], 
    sourceCurrency: 'USD'
});

/**
 * Посчитать стоимость корзины во всех валютах
 * @param {Array} selectedCart
 * @param {Array} currencies
 */
function calculatePrices({ selectedCart, currencies, sourceCurrency }) {
    // формируем параметры запроса для получения обменных курсов
    const query = getExchangeRatesQuery({ currencies, sourceCurrency });

    try {
        getExchangeRates(query)
            .then(exchangeRates => {
                // получаем итоговую стоимость корзины в дефолтной валюте
                const totalCardPrice = getTotalCartPrices(selectedCart);

                // получаем итоговую стоимость во всех валютах
                const totalCardPricesAllCurrencies = getPriceInAllCurrencies({ 
                    price: totalCardPrice, 
                    exchangeRates: exchangeRates.quotes,
                    sourceCurrency
                });
                console.log({ totalCardPricesAllCurrencies });

                // маппим имена валют
                const mappedCurrenciesNames = getMappedCurrenciesNames(totalCardPricesAllCurrencies);
                console.log({mappedCurrenciesNames});
            })
    } catch (error) {
        console.error('Не удалось получить курсы валют', { query },  error);
    }
}

/**
 * Получить корзину
 * @return {Array}
 */
function getSelectedCart() {
    return [
       { price: 20 },
       { price: 45 },
       { price: 67 },
       { price: 1305 }
    ];
}

/** 
 * Получить сумму цен товаров из корзины
 * @param {Array} selectedCard
 * @return {Number}
 */
function getTotalCartPrices(selectedCard) {
    return selectedCard.reduce((sum, product) => product.price ? sum + product.price : sum, 0);
}

/** 
 * Получить ценник во всех валютах
 * @param {Number} price
 * @param {Object} exchangeRates
 */
function getPriceInAllCurrencies({ price, exchangeRates, sourceCurrency }) {
    const result = {};
    for (let currency in exchangeRates) {
        // конвертируем ценник
        const convertedPrice = exchangeRates[currency] * price;
        // округляем до сотых
        const roundedPrice = Math.round(convertedPrice * 100) / 100;

        // убираем из названия валюты имя исходной валюты
        const key = currency.replace(sourceCurrency, '');

        result[key] = roundedPrice;
    }
    return result;
}

/**
 * Сформировать запрос для получения курсов
 * @param {Array} currencies 
 * @return {String}
 */
function getExchangeRatesQuery({ currencies = [], sourceCurrency }) {
    if (!currencies.length) {
        return ''
    }

    const host = 'http://www.apilayer.net/api/live';
    const query = querystring.stringify({
        access_key: '30313adc190d4966cde6f9d96abb53fa',
        format: 1,
        source: sourceCurrency,
        currencies: currencies.length ? currencies.join() : ''
    });

    console.log({ query });

    return `${host}?${query}`;
}

/** 
 * Получить курсы валют
 * @param {Object} query
 * @return {Promise<Object>}
 */
function getExchangeRates(query) {
    return new Promise((resolve, reject) => {
        const chunks = [];

        http
            .request(query, response => {
                response.on('data', chunk => chunks.push(chunk));
                response.on('end', () => resolve(
                    JSON.parse(Buffer.concat(chunks).toString())
                ));
            })
            .on('error', reject)
            .end();
    });
}

/**
 * Маппинг имен валют
 * @param {Object} prices 
 * @return {Object}
 */
function getMappedCurrenciesNames(prices) {
    const namesMap = {
        EUR: 'euros',
        JPY: 'yens',
        RUB: 'rubles',
        GBP: 'pounds'
    };
    
    const result = {};

    for (let item in prices) {
        const newName = namesMap[item];
        result[newName] = prices[item];
    } 
    return result;
}
